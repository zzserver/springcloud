# springcloud

#### 项目介绍
一个非常牛逼的springcloud项目示例

#### 软件架构说明


#一 开发环境

JDK8.0u133+IntelliJIDEA2017.1.4+Git2.10+Maven3.3.9

SpringBoot1.4.7.RELEASE+SpringCloud.Camden.SR7


#二 模块说明

springcloud-eureka-server 分布式服务注册中心(单点)

springcloud-eureka-server-ha 分布式服务注册中心(高可用版本)

springcloud-provider-user-service 用户服务提供者

springcloud-consumer-h5-ribbon 用户服务调用者，采用ribbon做客户端负载均衡

springcloud-consumer-h5-feign feign声明式服务调用者

springcloud-api-gateway 网关服务

springcloud-config-server 配置中心

springcloud-monitor-turbine 集群监控

spirngcloud-analyse-zipkin 链路跟踪

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
