package com.sinosoft.zzz.springcloudmonitorturbine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@EnableEurekaClient
@EnableTurbine
@EnableHystrixDashboard
@SpringBootApplication
public class SpringcloudMonitorTurbineApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudMonitorTurbineApplication.class, args);
	}
}
