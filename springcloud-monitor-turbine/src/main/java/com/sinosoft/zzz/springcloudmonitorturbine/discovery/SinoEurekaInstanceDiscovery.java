package com.sinosoft.zzz.springcloudmonitorturbine.discovery;

import com.netflix.discovery.EurekaClient;
import com.netflix.turbine.discovery.Instance;
import org.springframework.cloud.netflix.turbine.EurekaInstanceDiscovery;
import org.springframework.cloud.netflix.turbine.TurbineProperties;

import java.util.List;

/**
 * Created by Jason on 2017/8/25.
 */
public class SinoEurekaInstanceDiscovery extends EurekaInstanceDiscovery {
    public SinoEurekaInstanceDiscovery(TurbineProperties turbineProperties, EurekaClient eurekaClient) {
        super(turbineProperties, eurekaClient);
    }

    @Override
    protected List<Instance> getInstancesForApp(String serviceId) throws Exception {
        return super.getInstancesForApp(serviceId);
    }
}
